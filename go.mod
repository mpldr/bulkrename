module gitlab.com/poldi1405/bulkrename

go 1.14

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/jawher/mow.cli v1.2.0
	github.com/kr/text v0.2.0 // indirect
	github.com/mborders/logmatic v0.4.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/poldi1405/go-ansi v1.1.0
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
